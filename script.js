// 1)
// Функція для обчислення частки двох чисел
const divideNumbers = (num1, num2) => {
    if (num2 !== 0) {
        return num1 / num2;
    } else {
        return "Ділення на нуль неможливе";
    }
};

// Виклик функції та виведення результату в консоль
let number1 = 10;
let number2 = 2;
let quotient = divideNumbers(number1, number2);
console.log(`Частка чисел ${number1} і ${number2} =  ${quotient}`);


// 2)

// Функція для провалідації введеного користувачем числа
function validateNumberInput(input) {
    while (isNaN(input)) {
        input = prompt("Будь ласка, введіть число:");
    }
    return Number(input);
}

// Отримання чисел від користувача
let num1 = validateNumberInput(prompt("Будь ласка, введіть перше число:"));
let num2 = validateNumberInput(prompt("Будь ласка, введіть друге число:"));

// Отримання математичної операції від користувача
let operation = prompt("Будь ласка, введіть математичну операцію (+, -, *, /):");

// Провалідація математичної операції
if (operation !== "+" && operation !== "-" && operation !== "*" && operation !== "/") {
    alert("Такої операції не існує");
} else {
    // Функція для виконання математичної операції
    function calculate(num1, num2, operation) {
        switch (operation) {
            case "+":
                return num1 + num2;
            case "-":
                return num1 - num2;
            case "*":
                return num1 * num2;
            case "/":
                return num1 / num2;
            default:
                return "Невідома операція";
        }
    }

    // Виклик функції та виведення результату
    let result = calculate(num1, num2, operation);
    console.log(`Результат операції: ${result}`);
}

// 3)

// Функція для обчислення факторіалу числа
const calculateFactorial = (number) => {
    if (number === 0) {
        return 1;
    } else {
        return number * calculateFactorial(number - 1);
    }
};

// Отримання числа від користувача
let userNumber = parseInt(prompt("Будь ласка, введіть число для обчислення факторіалу:"));

// Виклик функції та виведення результату на екран
let factorialResult = calculateFactorial(userNumber);
console.log(`Факторіал числа ${userNumber} дорівнює: ${factorialResult}`);
